// Bài tập 1: Quản lý sinh viên

document.getElementById("txt-ket-qua").addEventListener("click", function(){
    
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;
    var diemMon1 = document.getElementById("txt-diem-1").value*1;
    var diemMon2 = document.getElementById("txt-diem-2").value*1;
    var diemMon3 = document.getElementById("txt-diem-3").value*1;
    var khuVuc = document.getElementById("txt-khu-vuc").value*1;
    var doiTuong = document.getElementById("txt-doi-tuong").value*1;
    var diemTong = 0;

    var tinhDiemTong = function(diemMon1,diemMon2,diemMon3, khuVuc, doiTuong){
        diemTong = diemMon1 + diemMon2 + diemMon3 + khuVuc + doiTuong;
    }

    if(diemMon1 <=0 || diemMon2 <=0 || diemMon3<=0){
        document.getElementById("txt-show-ket-qua").innerHTML=
        `
        <div>
        Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0
        </div>
        `
    }else{
        tinhDiemTong(diemMon1, diemMon2, diemMon3, khuVuc, doiTuong);
        if(diemTong>= diemChuan){
            document.getElementById("txt-show-ket-qua").innerHTML=
            `
            <div>
            Bạn đã đậu. Tổng điểm: ${diemTong}
            </div>
            `
        }else{
            document.getElementById("txt-show-ket-qua").innerHTML=
            `
            <div>
            Bạn đã rớt. Tổng điểm: ${diemTong}
            </div>
            `
        }
        
    }
    
});


// Bài tập 2: Tính Tiền điện
document.getElementById("txt-tinh-tien-dien").addEventListener("click", function(){
var hoVaTen = document.getElementById("txt-ho-ten").value;
var soKw = document.getElementById("txt-so-kw").value;
var tienDien = 0;
if(soKw <= 0){
    alert("Số kw không hợp lệ")
}else if(soKw> 0 && soKw <=50){
    tienDien = soKw * 500;
}else if(soKw<=100){
    tienDien = 50 * 500 + (soKw-50) * 650;
}else if(soKw <=200){
    tienDien = 50 * 500 + 50 * 650 + (soKw-100) * 850;
}else if(soKw <= 350){
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw-200)* 1100;
}else{
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
}
document.getElementById("txt-tien-dien").innerHTML=
`
<div>
Họ và Tên: ${hoVaTen} | Tiền điện là: ${tienDien}
</div>
`
});